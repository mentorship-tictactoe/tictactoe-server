﻿using Microsoft.EntityFrameworkCore;
using TicTacToe.Database.Configurations;
using TicTacToe.Models;

namespace TicTacToe.Database
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Rating> LeaderBoard { get; set; }

        public ApplicationContext()
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new AccountConfiguration());
            modelBuilder.ApplyConfiguration(new RatingConfiguration());
            modelBuilder.ApplyConfiguration(new StatisticConfiguration());

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\\DESKTOP-DR4D60S;Database=tictactoe;Trusted_Connection=True;");
        }
    }
}
