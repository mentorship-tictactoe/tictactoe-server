﻿using Serilog;

namespace TicTacToe.Logger
{
    public static class SerilogFactory
    {
        public static Serilog.Core.Logger CreateLogger()
        {
            return CreateConfiguration()
                .CreateLogger();
        }

        public static Serilog.Core.Logger CreateLogger(string eventType)
        {
            return CreateConfiguration()
                .Enrich.WithProperty("EventType", eventType)
                .CreateLogger();
        }

        private static LoggerConfiguration CreateConfiguration()
        {
            return new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.Seq("http://localhost:5341");
        }
    }
}
