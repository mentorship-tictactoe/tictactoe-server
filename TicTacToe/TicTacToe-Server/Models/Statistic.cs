﻿using System;

namespace TicTacToe.Models
{
    public class Statistic
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int Victories { get; set; }
        public int Defeats { get; set; }
        public int Draws { get; set; }
        public DateTime LastPlay { get; set; }
    }
}
