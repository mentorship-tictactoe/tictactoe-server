﻿namespace TicTacToe.Models
{
    public class Account
    {
        public int Id { get; set; }
        public string FacebookId { get; set; }
        public string FacebookToken { get; set; }
    }
}
